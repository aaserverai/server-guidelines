## Home

```plaintext
 _________________________________________
/ Hi there! If you are reading this page  \
| you have probably just being given      |
| access to the Server AI. In this wiki   |
| you can find all you need to properly   |
| employ the server.                	  |
|                                         |
\ Good luck!                              /
 -----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

## Server AI Mandatory Rules

- In your `home` you will find two folders: `Datasets` is suggested to store your data for training, while `Projects` should contain source codes, notebooks, and checkpoints.
- Install Slack on your devices and use the `#gpu` channel to coordinate with the other researchers working on the Server AI.
- **Before start working, please make sure to fill this Google Sheet table with your information concerning reserved machines: https://tinyurl.com/2dbcvuxn**

## GPUs and other resources

- Keep the resources under control: space occupied on disk (you can use the command `du -sh $HOME/*`), RAM and cores used (use `htop`), etc.
- After you launch every experiment (especially if your code is not stable), always make sure that it is not consuming too many resources (ram, cores, disk, etc.).
- If you need a lot of resources, please ask first to your leader. Experiments that use more resources than allowed will be killed.
- **To use a GPU you must first book it at this [this]( https://tinyurl.com/2dbcvuxn) address, you will find some rules on how to do it in the document. If you have any questions, please ask!**
- Use at most one GPU at a time. In case you need more, use the Slack `#gpus` channel to ask for permission and coordinate with the other users.

## How to write code on Server AI

- Python
  - **PyCharm**: learn [here](https://www.jetbrains.com/help/pycharm/creating-project-remotely.html) how to setup a remote project to directly write code on the server, execute python code remotely and launch docker containers directly within the IDE.
  - **vim**: you can write code directly from the server shell using vim. [Here](https://github.com/marcocannici/vim) you can find a pre-configured installation. If you have to make a quick modification to a file, this is the way to go.
  - **Jupyter Notebook**: we don't recommend writing code directly within notebooks (since this does not allow you to take advantage of basic IDE functionalities, e.g., code highlighting, error checks, refactoring and so on). However, Jupyter is a great tool for visualization so you may find it helpful in some cases. Read the dedicated section down below to learn how to properly set up a Docker image for that.

## How to launch experiments

- Use `tmux` to keep the session active even when you are disconnected from the server.
  If you never used it, read [this tutorial](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/) to learn all the basics and [this cheatsheet](https://gist.github.com/MohamedAlaa/2961058) for some advanced functionalities.
  
- Use the `watch nvidia-smi` command to monitor GPU usage.

- Use `htop` to keep resources under control.

- You must use Docker to run your experiments. For example, you can build an image as

  `docker build . -t <username>:<image_name>`

  and then run the related container as

  `docker run -it --gpus all -v <volume>:<root_folder> <username>:<image_name> `

  where `<volume>` represents the volume in which you want to run the container, `<root_folder>` the root folder inside the container, and `<image_name>` the name of the compiled image.

## Docker

Docker allows you to run experiments within containers, a sort of lightweight virtual machines. As in code you have classes and objects, in Docker you have **images** and **containers**. As classes, images can be extended to add more functionalities (i.e., applications, python packages, etc.). You can extend a basic image to add any package you want, in this guide you will learn how. [Here](https://hub.docker.com/r/airlab404/dl/tags) is a list of pre-installed images. Chose it depending on the python version, the deep learning framework and the cuda version you want to use.

### Quick Guide

This is just a cheatsheet to quickly introduce you to Docker. If you want to know more, these are some nice guides: https://docker-curriculum.com/ https://stackify.com/docker-tutorial/

- `docker run` and `nvidia-docker run` let you execute a container starting from an image. You have to use `nvidia-docker` to get access to gpus.
  - Run your first container with: `nvidia-docker run -ti --rm --name ${USER}_test --user $(id -u):$(id -g) airlab404/dl:cuda10_pytorch_py36 nvidia-smi`
    Congratulations, you have just run the `nvidia-smi` command inside a container using the *airlab404/dl:cuda10_pytorch_py36* image.
  - When running your experiments you have to specify the GPU to use. Since all the machines provide a single GPU unit, you can specify the `--gpus all ` command as
    `nvidia-docker run -ti --gpus all --rm --name ${USER}_test --user $(id -u):$(id -g) airlab404/dl:cuda10_pytorch_py36 nvidia-smi`
  - You can execute any program you want, e.g., your python program or even an interactive python console.
- `docker ps` let you see the list of running containers.
- `docker ps -a` also shows terminated containers. When you run a container always remember to specify the `--rm` flag so that this list doesn't grow.
- `docker stop <container_id>` stops a running container (you can only stop your own containers).
- `docker rm <container_id>` deletes a stopped container (you can only remove your own containers).
- `docker image list` shows the list of installed images.

### Restrictions

- To run a container with

  ```
  docker run
  ```

  and

  ```
  nvidia-docker run
  ```

  you have to:

  - Specify the `--user` flag with your uid and gid, i.e., `--user uid:gid`.
  - Specify a name for the container. The name must always start with your username followed by an underscore, e.g., `--name dolores_test`.

- You can pull any image from public repositories, however, you can only create images that:
  - Are tagged with a name that starts with your own username, e.g., `docker build -t dolores:latest`.

### Setting an env variable (PYTHONPATH)

In Python projects it is very common to set the `PYTHONPATH` env variable to the project root. If your code requires to do so, you can tell Docker to set it for you inside the container shell just before executing your program. Remember that the project folder will be mounted in the `/exp` folder, so you must export that folder! The arguments for `nvidia-docker` are

```plaintext
-v Datasets/mydataset:/data -e PYTHONPATH=/exp
```

### Exposing a port (Tensorboard)

You may need to run a program that requires to communicate via a certain network port. In order to do that, you have to tell Docker to expose the container port, i.e., to map the container port to a certain port on the host machine. You can do this by specifying the `-p <host_port>:<container_port>` argument in the `docker_args` field (you can use `-p <port>` if the host and container ports you want to map are the same, or `-p <host_port_start>-<host_port_end>:<container_port_start>-<container_port_end>` to map a range of ports). Note: you can only expose ports that are not already used by other containers!

### How to extend an image

Dockerfiles used to build *airlab-404* images can be found [here](https://gitlab.com/aaserverai/dockerfiles).

Let's say you want to use the *airlab404/dl:cuda10_pytorch_py36* image, but there are some python packages you have to use that are not included in the default image, e.g. `matplotlib`, `numba` and `pudb`. What you can do is to extend the basic image by adding the missing packages.

First, create a directory named `docker` in your project folder.
Inside that folder, create a file named `Dockerfile`:

```docker
FROM airlab404/dl:cuda10_pytorch_py36

# Install extras
COPY requirements.yml /requirements.yml
RUN /bin/bash -c "conda update conda"
# If you are using a py27 image, change this to py27
RUN /bin/bash -c ". activate py36 && conda env update -f=/requirements.yml"
CMD ["bash"]

WORKDIR /exp
```

and a file named `requirements.yml`:

```yaml
channels:
  - conda-forge
dependencies:
  - matplotlib 
  - numba
  - pudb
```

- Inside the `docker` folder, build your image by executing (replace with your own name):
  `docker build --rm -t <username>/dl:cuda10_pytorch_py36 .`
  You can use any tag you want after your username, but try to use a tag scheme which is easy to maintain.

If you are using `conda` and you already have an environment which is configured for your experiments, you can use conda to create the .yml file.

- activate your environment
- run `conda env export > requirements.yml`
- open the .yml file `vim requirements.yml`
- remove the lines starting with *name* and *prefix* (press `i` to enter Insert Mode in vim and delete the lines)
- save (press `ESC` and type `:wq`)

If you are not using conda you can use [pipreqs](https://github.com/bndr/pipreqs) to retrieve the list of imports. Follow the steps in the *pipreqs* github page, then replace any reference of `requirements.yml` with `requirements.txt` in your Dockefile, or convert the .txt file to the .yml syntax. You may also need to specify an additional `-n py36` argument in the `conda env update` command. Make sure to check the requirements list generated by pipreqs. Since it relies on import names, it may fail to identify a package if its name differs from the import definition (e.g., in most of the cases it will list Pytorch as *torch* and not as *pytorch*).

### Running Jupyter within a container

The basic images are not properly configured for Jupyter. However, you can build a personal image with some fixes to make it work.

You can access the jupyter notebook at the remote container from the local machine with the following command

`ssh -N -f -L localhost:<PORT>:localhost:<PORT> <username>@62.196.79.47 -p <ACCESS_PORT>`
